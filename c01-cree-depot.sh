cd /home/bernard/git-test
# Crée le dossier destiné à contenir la dodumentation et les sources du projet
mkdir c01
# Vas dans le dossier fraichement créé.
cd c01
# Initialise Git dans le dossier projet
git init
# Vérifie la bonne création du dépot local dans ".git"
ls -la
# Crée et remplis le fichier  "README.md"
echo "# C01: Crée dépot Git local" > README.md
# Met le fichier "README.md" dans la gestion des versions
git add README.md
# Publie les modifications dans le dépot Git local
git commit -m"init: crée README.md"

